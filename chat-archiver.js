const {Command} = require('commander');
const {version, description} = require('./package.json');
const fs = require('fs/promises');
const ngrok = require('ngrok');
const open = require('open');
const http = require('http');
const express = require('express');
const app = express();

const program = new Command();
program
	.name('chat-archiver')
	.description(description)
	.version(version, '-v, --version', 'output the current version')
	.requiredOption('--port <number>', 'port number', '8888')
	.requiredOption('--interface <ip>', 'bind to interface', '127.0.0.1')
	.option('--ngrok <token>', 'ngrok personal Authtoken');

program.showSuggestionAfterError();
program.showHelpAfterError('(add --help for additional information)');
program.parse();

const options = program.opts();
const ngrokOptions = new Object();
ngrokOptions.proto = 'http';
ngrokOptions.addr = options.port;
if (options.ngrok) ngrokOptions.authtoken = options.ngrok;

app.set('query parser', 'simple');
app.set('views', './');
app.set('view engine', 'pug');

app.get('/', async (req, res) => {
	res.render('index');
});

app.post(
	'/save',
	express.urlencoded({
		extended: true
	}),
	(req, res) => {
		console.log('saved settings');
		app.locals.webhook = req.body.webhook;
		res.redirect('/');
	}
);

app.post('/', express.json(), async (req, res) => {
	if (req.headers.authorization === app.locals.webhook) {
		res.sendStatus(200);
		try {
			if (typeof req.body.payload.operator !== 'undefined') {
				console.log('Saving chat message for ' + req.body.payload.operator);
				req.body.payload.object.event = req.body.event;
				try {
					await fs.mkdir('./messages', {recursive: true});
					await fs.appendFile(
						'./messages/' + req.body.payload.operator + '.txt',
						JSON.stringify(req.body.payload.object) + '\n'
					);
				} catch (e) {
					console.log(e);
				}
			}
		} catch (e) {}
	} else {
		res.sendStatus(401);
	}
});

function onError(error) {
	console.error('http | ' + error);
	process.exit(1);
}

function onListening() {
	console.log(
		'Local address: http://' + options.interface + ':' + options.port
	);
	ngrok.connect(ngrokOptions).then((url) => {
		console.log('Public address: ' + url);
		app.locals.url = url;
		open('http://' + options.interface + ':' + options.port);
	});
}

const server = http.createServer(app);
server.on('error', onError);
server.on('listening', onListening);
server.listen(options.port, options.interface);
