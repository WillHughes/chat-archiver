# Chat Archiver

Archive Zoom chat events.

This is a nodejs express application that will save Zoom chat events to a text file. 

The application starts a web server on port 8888 (default) and uses ngrok to make it accessible on the public internet.

# Installation

**Prerequisites:**
* [Zoom account](https://zoom.us)
* [Zoom Marketplace Account](https://marketplace.zoom.us/docs/guides)
* [Node.js 16+](https://nodejs.org/)

```
git clone https://gitlab.com/WillHughes/chat-archiver.git
cd chat-archiver
npm install
node ./chat-archiver.js
```

>For options see: `node ./chat-archiver.js --help`

# Configuration

1. Click `Build an App` on the Chat Archiver home page

![OAuth App Setup](1.png)

2. Create a new OAuth App

![OAuth App Setup](2.png)

3. Name the app and choose the app type.  

![OAuth App Setup](3.png)
> Disable publishing

4. Copy the URL from the Chat Archiver home page and paste it in for the `Redirect URL for OAuth` and `Add allow lists`.  

![OAuth App Setup](4.png)

5. On the `Information` tab enter:
    1. Short Description 
    2. Long Description
    3. Developer Name
    4. Developer Email Address

6. On the `Feature` tab copy the `Verification Token` and paste it into the Chat Archiver home page.  Click `Save`.

7. Add a new event subscription

![OAuth App Setup](5.png)

8. Copy the URL from the Chat Archiver home page and paste it in for the `Event notification endpoint URL`.

9. Click add events and select the `Chat Message` events

![OAuth App Setup](6.png)

10. Click `Save` to save the new event subscription.

11.  On the `Activation` tab click `Add`.

12. Send chat messages.

# Where are my messages?
Messages will be saved to `chat-archiver/messages/<user email>.txt`, where \<user email\> is a users' email address.

